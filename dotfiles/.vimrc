
" Defaults
set number
set nohlsearch

" Indent
set expandtab
set smartindent
set autoindent
set shiftwidth=4
set tabstop=4

" Colors
syntax on
colorscheme mustang
set background=dark

" Headers
autocmd bufnewfile *.h so /home/Rémi/.vim/c_header.txt
autocmd bufnewfile *.h exe "1," . 7 . "g/File Name :.*/s//File Name : " .expand("%")
autocmd bufnewfile *.c so /home/Rémi/.vim/c_header.txt
autocmd bufnewfile *.c exe "1," . 7 . "g/File Name :.*/s//File Name : " .expand("%")
