# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"

# ZSH THEME
ZSH_THEME="minimal"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
export UPDATE_ZSH_DAYS=15

# Uncomment the following line to display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git)

source $ZSH/oh-my-zsh.sh

# USER CONFIGURATION 

rmd () {
  pandoc $1 | lynx -stdin
}

# ALIASES

# MAKE
alias md='make debug > /dev/null'
alias mk='make > /dev/null'
alias mlk='make > /dev/null'
alias mf='make fclean > /dev/null'
alias mc='make clean > /dev/null'

# GIT
alias shortlog='git shortlog'
alias s='git status'
alias co='git checkout'

# LS
alias l='/bin/ls --color=auto'
alias sl='/bin/ls --color=auto'
alias lls='/bin/ls --color=auto'
alias ms='/bin/ls --color=auto'
alias ls='/bin/ls --color=auto'
alias ls='/bin/ls --color=auto' 

alias ll='tree -a -I .git'

# CLEAR 
alias cleare='clear'
alias claer='clear'
alias lcear='clear'
alias clearls='clear && ls'
alias clera='clear'
alias celra='clear'
alias lcera='clear'

# KEYBOARD
alias fr='setxkbmap fr'
alias us='setxkbmap us'

# UTILITY
alias ne="emacs -nw"
alias katk='xrandr --output HDMI-2 --mode 1920x1080'
alias rc='vi ~/.zshrc'
alias refresh='source ~/.zshrc'

# MODIFIED PATH
export CONFIG=$HOME/config
export SCRIPTS=$CONFIG/scripts
export TEMPLATES=$CONFIG/templates
